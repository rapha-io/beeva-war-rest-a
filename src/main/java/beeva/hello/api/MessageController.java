package beeva.hello.api;

import java.util.UUID;

import beeva.hello.model.Message;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    private static final String messageWrapper = "Hello, %s!";

    @RequestMapping("/hello")
    public Message sayHello(@RequestParam(value="visitor", defaultValue="from Beeva") String visitor) {
        return new Message(UUID.randomUUID().toString(),
                            String.format(messageWrapper, visitor));
    }
}
