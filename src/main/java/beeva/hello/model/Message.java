package beeva.hello.model;

public class Message {

    private final String requestId;
    private final String messageContent;

    public Message(String requestId, String messageContent) {
        this.requestId = requestId;
        this.messageContent = messageContent;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getMessageContent() {
        return messageContent;
    }
}
